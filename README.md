Role Name
=========

This role install and configures wireguard interface wg0 for your environment

Requirements
------------

To be able to setup a wireguard connection, you need to have configured a wireguard peer (endpoint) to connect with and you need to generate a private-public key pair for the local system.

Create a local private and public keypair:
```bash
wg genkey | tee privatekey | wg pubkey > publickey
```
Set the privatekey and publickey in the right variables before deploying a new node.

Role Variables
--------------
This role has no defaults, all variables have to be set in your ansible playbook

```yaml
# host_vars specific
wireguard_local_private_key: the generated private key for local connection
wireguard_local_public_key: the public key for the local connection. Needed to setup an andpoint on your peer
wireguard_local_ip_address: the IP address assigned by the peer for this connection
# Group_vars
wireguard_peer_ip_address: IP address of the wireguard peer
wireguard_peer_port: port for wireguard to connect with
wireguard_peer_public_key: the public key from the peer. Generated when you setup your wireguard peer
wireguard_peer_allowed_ips: array of allowed IPs
# Optional settings
wireguard_interface_dns_servers: array of dns servers specific for this interface
wireguard_interface_dns_search_domain: string with the search domain for this interface
wireguard_interface_routes: sets routes for this interface (e.g. 10.10.0.1/32)
```

Dependencies
------------


Example Playbook
----------------
- name: Testing
  hosts: all
  tasks:
    - name: Wireguard
      include_role:
        name: wireguard
        apply:
          tags: wireguard

License
-------

Apache-2.0
